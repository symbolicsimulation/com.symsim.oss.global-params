;;;; com.symsim.oss.global-params.asd

(asdf:defsystem #:com.symsim.oss.global-params
  :description "Describe com.symsim.oss.global-params here"
  :author "Your Name <your.name@example.com>"
  :license  "Golden Rule License"
  :version "0.0.1"
  :serial t
  :depends-on (:cl-singleton-mixin)
  :components ((:file "package")
               (:file "com.symsim.oss.global-params")))
