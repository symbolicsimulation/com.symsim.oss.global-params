# com.symsim.oss.global-params
### _John Morrison <jm@symbolic-simulation.com>_

Make singleton global parameters, optionally backed by environment variables.

* First choice is an explictly-set value
* Second choice is the value of the environment variable
* Third choice is the default value

## License

[The Golden Rule License](https://www.symbolic-simulation.com/the-golden-rule-license)

### Who so I talk to? ###

* [John Morrison](jm@symbolic-simulation.com)

### How do I use this? ###
```
(ql:quickload :com.symsim.oss.global-params)

(use-package :com.symsim.oss.global-params)

(defclass p1-class (global-parameter) ())

(defvar *p1* (make-instance
	      'p1-class
	      :param-type 'integer
	      :envvar "P1"
	      :defaultval 111
	      :user-doc "int-param documentation"))
(describe *p1* *terminal-io*)
(format t "(value-of *p1*) = ~s~%" (value-of *p1*))
(sb-posix:setenv "P1" "333" 1)
(format t "(value-of *p1*) = ~s~%" (value-of *p1*))
(override *p1* 222)
(format t "(value-of *p1*) = ~s~%" (value-of *p1*))

(defclass p2-class (global-parameter) ())

(defvar *p2* (make-instance
	      'p2-class
	      :envvar "P2"
	      :defaultval "twotwotwo"
	      :user-doc "string-param documentation"))
(describe *p2* *terminal-io*)
(format t "(value-of *p2*) = ~s~%" (value-of *p2*))
```