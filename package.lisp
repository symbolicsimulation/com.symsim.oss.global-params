;;;; package.lisp

(defpackage #:com.symsim.oss.global-params
  (:use
   #:cl
   #:cl-singleton-mixin)
  (:export
   #:global-parameter
   #:override
   #:value-of
   )
  )
