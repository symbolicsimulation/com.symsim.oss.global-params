;;;; com.symsim.oss.global-params.lisp

(in-package #:com.symsim.oss.global-params)

;; Basic param backed by environment variable

(defclass global-parameter (singleton-mixin)
  ((envvar :accessor envvar :initarg :envvar :initform nil)
   (defaultval :accessor defaultval :initarg :defaultval :initform nil) ; Should probably check type-of this
   (realval :accessor realval :initform nil)
   (param-type :accessor param-type :initarg :param-type :initform 'string) ; Should probably complain if unknown
   (user-doc :accessor user-doc :initarg :user-doc :initform nil)))

(defmethod override ((self global-parameter) new-value)
  (cond ((subtypep (type-of new-value) (param-type self))
	 (setf (realval self) new-value))
	(t
	 (warn (format nil "param ~a type is ~s, new-value is of type ~s"
		       (class-name (find-class (type-of self)))
		       (param-type self)
		       (type-of new-value))))))

(defmethod get-env-val ((self global-parameter))
  (cond ((null (envvar self))		; No env var to check
	 (warn (format nil "global-parameter ~a unset, no environment variable either" (class-name (find-class (type-of self)))))
	 nil)
	((null (uiop/os:getenv (envvar self))) ; Env var exists, but unset
	 (warn (format nil "global-parameter ~a unset, no value in env var ~s" (class-name (find-class (type-of self))) (envvar self))))
	(t
	 (uiop/os:getenv (envvar self)))))

(defmethod value-of ((self global-parameter))
  (cond ((realval self)
	 (realval self))
	((get-env-val self)		; Coerce string value of any envvar appropriately
	 (case (param-type self)
	   (boolean (read-from-string (get-env-val self)))
	   (integer (parse-integer (get-env-val self)))
	   (string (get-env-val self))
	   (t
 	    (error (format		; Throw an error?
		    nil
		    "global-parameter ~a of unknown-type ~s"
		    (class-name (find-class (type-of self)))
		    (param-type self)))
	    nil)))
	((defaultval self)		; No explicit value, neither envvar
	 (defaultval self))
	(t
	 (warn (format
		nil
		"global-parameter ~a unset, no environment variable, no default value either"
		(class-name (find-class (type-of self)))))
	 nil)))

      
